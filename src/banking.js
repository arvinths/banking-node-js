function deposit(user, amount) {
    if (amount <= 0) {
        throw new Error("Invalid amount for deposit");
    }
    user.balance += amount;
    return user.balance;
}

function withdraw(user, amount) {
    if (amount <= 0 || amount > user.balance) {
        throw new Error("Invalid amount for withdrawal");
    }
    user.balance -= amount;
    return user.balance;
}

function transfer(fromUser, toUser, amount) {
    if (amount <= 0 || amount > fromUser.balance) {
        throw new Error("Invalid transfer amount or insufficient balance");
    }
    fromUser.balance -= amount;
    toUser.balance += amount;
    return { senderBalance: fromUser.balance, recipientBalance: toUser.balance };
}

module.exports = {
    deposit,
    withdraw,
    transfer
};
